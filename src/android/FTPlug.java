package com.terzettoinfotech.cordova.ftp;

//package android;

import org.apache.commons.net.PrintCommandListener;
import org.apache.commons.net.ftp.*;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.util.Calendar;
import java.util.Date;
import java.util.Formatter;
import java.util.TimeZone;

public class FTPlug extends CordovaPlugin {
    private static final String LOG_TAG = "FTPlug";

    @Override
    public boolean execute(String action, final JSONArray args_data, final CallbackContext callbackContext) {
        final FTPClient ftp = new FTPClient();
        final FTPClientConfig config = new FTPClientConfig();

        try {
            final JSONObject args = args_data.getJSONObject(0);
            final String ftphost = args.getString("ftphost");
            final String port = args.getString("port") == null || args.getString("port").equals("") ? "21" : args.getString("port");
            final String username = args.getString("username") == null || args.getString("username").equals("") ? "anonymous" : args.getString("username");
            final String password = args.getString("password") == null || args.getString("password").equals("") ? "anonymous" : args.getString("password");
            final String pathname = args.getString("pathname") == null || args.getString("pathname").equals("") ? "/" : args.getString("pathname");

            PluginResult.Status status = PluginResult.Status.OK;

            ftp.setControlEncoding("UTF-8");

            ftp.addProtocolCommandListener(new PrintCommandListener(new PrintWriter(System.out), true));

            //if (localActive)
            //    ftp.enterLocalActiveMode();
            //else
            ftp.enterLocalPassiveMode();

            //config.setRecentDateFormatStr("");
            //ftp.configure(config);

            ftp.connect(ftphost, Integer.parseInt(port));

            final int reply = ftp.getReplyCode();

            if (!FTPReply.isPositiveCompletion(reply)) {
                ftp.disconnect();
                throw new IOException("FTP server refused connection.");
            }

            if (action.equalsIgnoreCase("listnames")) {
                final String remote = args.getString("remote") == null || args.getString("remote").equals("") ? "/" : args.getString("remote");
                cordova.getThreadPool().execute(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (!ftp.login(username, password)) {
                                ftp.logout();
                            }
                            ftp.setUseEPSVwithIPv4(false);
                            final JSONArray result = new JSONArray();

                            for (String s : ftp.listNames(remote)) {
                                result.put(s);
                            }

                            callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, result));
                        } catch (IOException e) {
                            callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.IO_EXCEPTION, e.getMessage()));
                        } catch (Exception e) {
                            callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.MALFORMED_URL_EXCEPTION, e.getMessage()));
                        } finally {
                            try {
                                ftp.logout();
                                ftp.disconnect();
                            } catch (Exception e) {
                            }
                        }
                    }
                });

                PluginResult pluginResult = new PluginResult(PluginResult.Status.NO_RESULT);
                pluginResult.setKeepCallback(true);
                return true;
            }

            if (action.equalsIgnoreCase("mkdir")) {
                final String dirname = args.getString("dirpath") == null || args.getString("dirpath").equals("") ? null : args.getString("dirpath");
                cordova.getThreadPool().execute(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (dirname == null) throw new IOException("Invalid directory name.");
                            if (!ftp.login(username, password)) {
                                ftp.logout();
                            }
                            ftp.setUseEPSVwithIPv4(false);
                            boolean result = ftp.makeDirectory(dirname);
                            callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, result));
                        } catch (IOException e) {
                            callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.IO_EXCEPTION, e.getMessage()));
                        } catch (Exception e) {
                            callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.MALFORMED_URL_EXCEPTION, e.getMessage()));
                        } finally {
                            try {
                                ftp.logout();
                                ftp.disconnect();
                            } catch (Exception e) {
                            }
                        }
                    }
                });

                PluginResult pluginResult = new PluginResult(PluginResult.Status.NO_RESULT);
                pluginResult.setKeepCallback(true);
                return true;
            }

            if (action.equalsIgnoreCase("deletefile")) {
                final String filename = args.getString("filename") == null || args.getString("filename").equals("") ? null : args.getString("filename");
                cordova.getThreadPool().execute(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (filename == null) throw new IOException("File name required.");

                            if (!ftp.login(username, password)) {
                                ftp.logout();
                            }
                            ftp.setUseEPSVwithIPv4(false);
                            boolean result = ftp.deleteFile(filename);

                            callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, result));
                        } catch (IOException e) {
                            callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.IO_EXCEPTION, e.getMessage()));
                        } catch (Exception e) {
                            callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.MALFORMED_URL_EXCEPTION, e.getMessage()));
                        } finally {
                            try {
                                ftp.logout();
                                ftp.disconnect();
                            } catch (Exception e) {
                            }
                        }
                    }
                });

                PluginResult pluginResult = new PluginResult(PluginResult.Status.NO_RESULT);
                pluginResult.setKeepCallback(true);
                return true;
            }

            if (action.equalsIgnoreCase("rename")) {
                final String from = args.getString("from") == null || args.getString("from").equals("") ? null : args.getString("from");
                final String to = args.getString("to") == null || args.getString("to").equals("") ? null : args.getString("to");
                cordova.getThreadPool().execute(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (from == null) throw new IOException("First file name required.");
                            if (to == null) throw new IOException("Second file name required.");

                            if (!ftp.login(username, password)) {
                                ftp.logout();
                            }
                            ftp.setUseEPSVwithIPv4(false);
                            boolean result = ftp.rename(from, to);
                            callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, result));
                        } catch (IOException e) {
                            callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.IO_EXCEPTION, e.getMessage()));
                        } catch (Exception e) {
                            callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.MALFORMED_URL_EXCEPTION, e.getMessage()));
                        } finally {
                            try {
                                ftp.logout();
                                ftp.disconnect();
                            } catch (Exception e) {
                            }
                        }
                    }
                });

                PluginResult pluginResult = new PluginResult(PluginResult.Status.NO_RESULT);
                pluginResult.setKeepCallback(true);
                return true;
            }

            if (action.equalsIgnoreCase("deletedir")) {
                final String filename = args.getString("dirname") == null || args.getString("dirname").equals("") ? null : args.getString("dirname");
                cordova.getThreadPool().execute(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (filename == null) throw new IOException("File name required.");

                            if (!ftp.login(username, password)) {
                                ftp.logout();
                            }
                            ftp.setUseEPSVwithIPv4(false);
                            boolean result = ftp.removeDirectory(filename);
                            callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, result));
                        } catch (IOException e) {
                            callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.IO_EXCEPTION, e.getMessage()));
                        } catch (Exception e) {
                            callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.MALFORMED_URL_EXCEPTION, e.getMessage()));
                        } finally {
                            try {
                                ftp.logout();
                                ftp.disconnect();
                            } catch (Exception e) {
                            }
                        }
                    }
                });

                PluginResult pluginResult = new PluginResult(PluginResult.Status.NO_RESULT);
                pluginResult.setKeepCallback(true);
                return true;
            }


            if (action.equalsIgnoreCase("upload")) {

                final String local = args.getString("local") == null || args.getString("local").equals("") ? null : args.getString("local");
                final String remote = args.getString("remote") == null || args.getString("remote").equals("") ? null : args.getString("remote");

                cordova.getThreadPool().execute(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (local == null || remote == null) throw new IOException("Invalid path.");
                            ftp.setUseEPSVwithIPv4(false);
                            ftp.setFileType(FTP.BINARY_FILE_TYPE);
                            if (!ftp.login(username, password)) {
                                ftp.logout();
                            }
                            InputStream input;
                            input = new FileInputStream(local);
                            boolean result = ftp.storeFile(remote, input);

                            input.close();
                            callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, result));
                        } catch (IOException e) {
                            callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.IO_EXCEPTION, e.getMessage()));
                        } catch (Exception e) {
                            callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.MALFORMED_URL_EXCEPTION, e.getMessage()));
                        } finally {
                            try {
                                ftp.logout();
                                ftp.disconnect();
                            } catch (IOException e) {

                            }
                        }
                    }
                });

                PluginResult pluginResult = new PluginResult(PluginResult.Status.NO_RESULT);
                pluginResult.setKeepCallback(true);
                return true;
            }


            if (action.equalsIgnoreCase("download")) {
                final String local = args.getString("local") == null || args.getString("local").equals("") ? null : args.getString("local");
                final String remote = args.getString("remote") == null || args.getString("remote").equals("") ? null : args.getString("remote");
                cordova.getThreadPool().execute(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (local == null || remote == null) throw new IOException("Invalid path.");
                            ftp.setUseEPSVwithIPv4(false);
                            ftp.setFileType(FTP.BINARY_FILE_TYPE);
                            if (!ftp.login(username, password)) {
                                ftp.logout();
                            }

                            OutputStream output;
                            output = new FileOutputStream(local);
                            boolean result = ftp.retrieveFile(remote, output);
                            output.close();
                            callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, result));
                        } catch (IOException e) {
                            callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.IO_EXCEPTION, e.getMessage()));
                        } catch (Exception e) {
                            callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.MALFORMED_URL_EXCEPTION, e.getMessage()));
                        } finally {
                            try {
                                ftp.logout();
                                ftp.disconnect();
                            } catch (IOException e) {

                            }
                        }
                    }
                });

                PluginResult pluginResult = new PluginResult(PluginResult.Status.NO_RESULT);
                pluginResult.setKeepCallback(true);
                return true;
            }

            if (action.equalsIgnoreCase("list")) {
                final String path_name;
                if (args.getString("remote") != null && !args.getString("remote").equals(""))
                    path_name = args.getString("remote");
                else path_name = pathname;
                cordova.getThreadPool().execute(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (!ftp.login(username, password)) {
                                ftp.logout();
                            }
                            ftp.setUseEPSVwithIPv4(false);
                            final JSONArray result = new JSONArray();
                            for (FTPFile ftpFile : ftp.mlistDir(path_name)) {
                                JSONObject object = new JSONObject();
                                object.put("name", ftpFile.getName());
                                object.put("link", ftpFile.getLink());
                                object.put("path", path_name + "/" + ftpFile.getName());
                                object.put("hardlinkcount", ftpFile.getHardLinkCount());
                                object.put("modify", midify(ftpFile.getTimestamp()));
                                String type = "";
                                switch (ftpFile.getType()) {
                                    case 0:
                                        type = "File";
                                        break;
                                    case 1:
                                        type = "Dir";
                                        break;
                                    case 2:
                                        type = "Link";
                                        break;
                                    case 3:
                                        type = "UNK";
                                        break;
                                }
                                object.put("type", type);
                                object.put("size", ftpFile.getSize());
                                object.put("user", ftpFile.getUser());
                                object.put("group", ftpFile.getGroup());
                                object.put("permission", permissionToString(ftpFile, ftpFile.USER_ACCESS) + permissionToString(ftpFile, ftpFile.GROUP_ACCESS) + permissionToString(ftpFile, ftpFile.WORLD_ACCESS));
                                result.put(object);
                            }
                            callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, result));
                        } catch (JSONException e) {
                            callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.JSON_EXCEPTION, e.getMessage()));
                        } catch (IOException e) {
                            callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.IO_EXCEPTION, e.getMessage()));
                        } catch (Exception e) {
                            callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.MALFORMED_URL_EXCEPTION, e.getMessage()));
                        } finally {
                            try {
                                ftp.logout();
                                ftp.disconnect();
                            } catch (IOException e) {

                            }
                        }
                    }

                    private String midify(Calendar timestamp) {
                        StringBuilder sb = new StringBuilder();
                        Formatter fmt = new Formatter(sb);
                        if (timestamp != null) {
                            String timezone = Calendar.getInstance().getTimeZone().toString();
                            TimeZone newZone = TimeZone.getTimeZone(timezone);
                            if (!newZone.equals(timestamp.getTimeZone())) {
                                Date original = timestamp.getTime();
                                Calendar newStamp = Calendar.getInstance(newZone);
                                newStamp.setTime(original);
                                timestamp = newStamp;
                            }

                            fmt.format("%1$tY-%1$tm-%1$td", timestamp);
                            if (timestamp.isSet(Calendar.HOUR_OF_DAY)) {
                                fmt.format(" %1$tH", timestamp);
                                if (timestamp.isSet(Calendar.MINUTE)) {
                                    fmt.format(":%1$tM", timestamp);
                                    if (timestamp.isSet(Calendar.SECOND)) {
                                        fmt.format(":%1$tS", timestamp);
                                        if (timestamp.isSet(Calendar.MILLISECOND)) {
                                            fmt.format(".%1$tL", timestamp);
                                        }
                                    }
                                }
                                fmt.format(" %1$tZ", timestamp);
                            }
                        }
                        fmt.close();
                        return sb.toString();
                    }

                    private String permissionToString(FTPFile ftpFile, int access) {
                        StringBuilder sb = new StringBuilder();
                        if (ftpFile.hasPermission(access, ftpFile.READ_PERMISSION)) {
                            sb.append('r');
                        } else {
                            sb.append('-');
                        }
                        if (ftpFile.hasPermission(access, ftpFile.WRITE_PERMISSION)) {
                            sb.append('w');
                        } else {
                            sb.append('-');
                        }
                        if (ftpFile.hasPermission(access, ftpFile.EXECUTE_PERMISSION)) {
                            sb.append('x');
                        } else {
                            sb.append('-');
                        }
                        return sb.toString();
                    }
                });

                PluginResult pluginResult = new PluginResult(PluginResult.Status.NO_RESULT);
                pluginResult.setKeepCallback(true);
                return true;
            }

            if (action.equalsIgnoreCase("recursivelist")) {

                final String path = args.getString("path") == null || args.getString("path").equals("") ? pathname : args.getString("path");

                cordova.getThreadPool().execute(new Runnable() {
                    final private JSONArray result = new JSONArray();
                    @Override
                    public void run() {
                        try {
                            if (!ftp.login(username, password)) {
                                ftp.logout();
                            }

                            ftp.setUseEPSVwithIPv4(false);
                            ftp.setFileType(FTP.BINARY_FILE_TYPE);
                            listDirectory(ftp, path, "", result);
                            callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, result));
                        } catch (IOException e) {
                            callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.IO_EXCEPTION, e.getMessage()));
                        } catch (Exception e) {
                            callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.MALFORMED_URL_EXCEPTION, e.getMessage()));
                        } finally {
                            try {
                                ftp.logout();
                                ftp.disconnect();
                            } catch (IOException e) {

                            }
                        }
                    }

                    void listDirectory(FTPClient ftpClient, String parentDir, String currentDir, JSONArray sub) throws IOException {
                        String dirToList = parentDir;
                        if (!currentDir.equals("")) {
                            dirToList += "/" + currentDir;
                        }
                        FTPFile[] subFiles = ftpClient.listFiles(dirToList);
                        if (subFiles != null && subFiles.length > 0) for (FTPFile ftpFile : subFiles) {
                            String currentFileName = ftpFile.getName();
                            if (currentFileName.equals(".") || currentFileName.equals("..")) {
                                continue;
                            }
                            JSONObject object = new JSONObject();
                            if (ftpFile.isDirectory()) {
                                try {
                                    JSONArray subdir = new JSONArray();
                                    object.put("size", ftpFile.getSize());
                                    object.put("user", ftpFile.getUser());
                                    object.put("name", ftpFile.getName());
                                    object.put("link", ftpFile.getLink());
                                    object.put("path", dirToList + "/" + ftpFile.getName());
                                    object.put("hardlinkcount", ftpFile.getHardLinkCount());
                                    object.put("modify", midify(ftpFile.getTimestamp()));
                                    String type = "Dir";
                                    object.put("type", type);
                                    object.put("group", ftpFile.getGroup());
                                    object.put("permission", permissionToString(ftpFile, ftpFile.USER_ACCESS) + permissionToString(ftpFile, ftpFile.GROUP_ACCESS) + permissionToString(ftpFile, ftpFile.WORLD_ACCESS));
                                    object.put("subdir", subdir);
                                    sub.put(object);
                                    listDirectory(ftpClient, dirToList, currentFileName, subdir);
                                } catch (JSONException e1) {
                                    e1.printStackTrace();
                                }
                            } else {
                                try {
                                    String[] ext = ftpFile.getName().split("\\.");
                                    if (ext[ext.length - 1].equalsIgnoreCase("jpg") || ext[ext.length - 1].equalsIgnoreCase("png") || ext[ext.length - 1].equalsIgnoreCase("jpeg")) {
                                        object.put("extension", ext[ext.length - 1]);
                                        object.put("name", ftpFile.getName());
                                        object.put("link", ftpFile.getLink());
                                        object.put("path", dirToList + "/" + ftpFile.getName());
                                        object.put("hardlinkcount", ftpFile.getHardLinkCount());
                                        object.put("modify", midify(ftpFile.getTimestamp()));
                                        String type = "";
                                        switch (ftpFile.getType()) {
                                            case 0:
                                                type = "File";
                                                break;
                                            case 1:
                                                type = "Dir";
                                                break;
                                            case 2:
                                                type = "Link";
                                                break;
                                            case 3:
                                                type = "UNK";
                                                break;
                                        }
                                        object.put("type", type);
                                        object.put("size", ftpFile.getSize());
                                        object.put("user", ftpFile.getUser());
                                        object.put("group", ftpFile.getGroup());
                                        object.put("permission", permissionToString(ftpFile, ftpFile.USER_ACCESS) + permissionToString(ftpFile, ftpFile.GROUP_ACCESS) + permissionToString(ftpFile, ftpFile.WORLD_ACCESS));
                                        //object.put("subdir", subdir);
                                        sub.put(object);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }

                    private String midify(Calendar timestamp) {
                        StringBuilder sb = new StringBuilder();
                        Formatter fmt = new Formatter(sb);
                        if (timestamp != null) {
                            String timezone = Calendar.getInstance().getTimeZone().toString();
                            TimeZone newZone = TimeZone.getTimeZone(timezone);
                            if (!newZone.equals(timestamp.getTimeZone())) {
                                Date original = timestamp.getTime();
                                Calendar newStamp = Calendar.getInstance(newZone);
                                newStamp.setTime(original);
                                timestamp = newStamp;
                            }

                            fmt.format("%1$tY-%1$tm-%1$td", timestamp);
                            if (timestamp.isSet(Calendar.HOUR_OF_DAY)) {
                                fmt.format(" %1$tH", timestamp);
                                if (timestamp.isSet(Calendar.MINUTE)) {
                                    fmt.format(":%1$tM", timestamp);
                                    if (timestamp.isSet(Calendar.SECOND)) {
                                        fmt.format(":%1$tS", timestamp);
                                        if (timestamp.isSet(Calendar.MILLISECOND)) {
                                            fmt.format(".%1$tL", timestamp);
                                        }
                                    }
                                }
                                fmt.format(" %1$tZ", timestamp);
                            }
                        }
                        fmt.close();
                        return sb.toString();
                    }

                    private String permissionToString(FTPFile ftpFile, int access) {
                        StringBuilder sb = new StringBuilder();
                        if (ftpFile.hasPermission(access, ftpFile.READ_PERMISSION)) {
                            sb.append('r');
                        } else {
                            sb.append('-');
                        }
                        if (ftpFile.hasPermission(access, ftpFile.WRITE_PERMISSION)) {
                            sb.append('w');
                        } else {
                            sb.append('-');
                        }
                        if (ftpFile.hasPermission(access, ftpFile.EXECUTE_PERMISSION)) {
                            sb.append('x');
                        } else {
                            sb.append('-');
                        }
                        return sb.toString();
                    }

                });

                PluginResult pluginResult = new PluginResult(PluginResult.Status.NO_RESULT);
                pluginResult.setKeepCallback(true);
                return true;
            }

            if (action.equalsIgnoreCase("recursivedirlist")) {

                final String path = args.getString("path") == null || args.getString("path").equals("") ? pathname : args.getString("path");

                cordova.getThreadPool().execute(new Runnable() {
                    final private JSONArray result = new JSONArray();
                    @Override
                    public void run() {
                        try {
                            if (!ftp.login(username, password)) {
                                ftp.logout();
                            }

                            ftp.setUseEPSVwithIPv4(false);
                            ftp.setFileType(FTP.BINARY_FILE_TYPE);
                            listDirectory(ftp, path, "", result);
                            callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, result));
                        } catch (IOException e) {
                            callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.IO_EXCEPTION, e.getMessage()));
                        } catch (Exception e) {
                            callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.MALFORMED_URL_EXCEPTION, e.getMessage()));
                        } finally {
                            try {
                                ftp.logout();
                                ftp.disconnect();
                            } catch (IOException e) {

                            }
                        }
                    }

                    void listDirectory(FTPClient ftpClient, String parentDir, String currentDir, JSONArray sub) throws IOException {
                        String dirToList = parentDir;
                        if (!currentDir.equals("")) {
                            dirToList += "/" + currentDir;
                        }
                        FTPFile[] subFiles = ftpClient.listFiles(dirToList);
                        if (subFiles != null && subFiles.length > 0) for (FTPFile ftpFile : subFiles) {
                            String currentFileName = ftpFile.getName();
                            if (currentFileName.equals(".") || currentFileName.equals("..")) {
                                continue;
                            }
                            JSONObject object = new JSONObject();
                            if (ftpFile.isDirectory()) {
                                try {
                                    JSONArray subdir = new JSONArray();
                                    object.put("size", ftpFile.getSize());
                                    object.put("user", ftpFile.getUser());
                                    object.put("name", ftpFile.getName());
                                    object.put("link", ftpFile.getLink());
                                    object.put("path", dirToList + "/" + ftpFile.getName());
                                    object.put("hardlinkcount", ftpFile.getHardLinkCount());
                                    object.put("modify", midify(ftpFile.getTimestamp()));
                                    String type = "Dir";
                                    object.put("type", type);
                                    object.put("group", ftpFile.getGroup());
                                    object.put("permission", permissionToString(ftpFile, ftpFile.USER_ACCESS) + permissionToString(ftpFile, ftpFile.GROUP_ACCESS) + permissionToString(ftpFile, ftpFile.WORLD_ACCESS));
                                    object.put("subdir", subdir);
                                    sub.put(object);
                                    listDirectory(ftpClient, dirToList, currentFileName, subdir);
                                } catch (JSONException e1) {
                                    e1.printStackTrace();
                                }
                            }
                        }
                    }

                    private String midify(Calendar timestamp) {
                        StringBuilder sb = new StringBuilder();
                        Formatter fmt = new Formatter(sb);
                        if (timestamp != null) {
                            String timezone = Calendar.getInstance().getTimeZone().toString();
                            TimeZone newZone = TimeZone.getTimeZone(timezone);
                            if (!newZone.equals(timestamp.getTimeZone())) {
                                Date original = timestamp.getTime();
                                Calendar newStamp = Calendar.getInstance(newZone);
                                newStamp.setTime(original);
                                timestamp = newStamp;
                            }

                            fmt.format("%1$tY-%1$tm-%1$td", timestamp);
                            if (timestamp.isSet(Calendar.HOUR_OF_DAY)) {
                                fmt.format(" %1$tH", timestamp);
                                if (timestamp.isSet(Calendar.MINUTE)) {
                                    fmt.format(":%1$tM", timestamp);
                                    if (timestamp.isSet(Calendar.SECOND)) {
                                        fmt.format(":%1$tS", timestamp);
                                        if (timestamp.isSet(Calendar.MILLISECOND)) {
                                            fmt.format(".%1$tL", timestamp);
                                        }
                                    }
                                }
                                fmt.format(" %1$tZ", timestamp);
                            }
                        }
                        fmt.close();
                        return sb.toString();
                    }

                    private String permissionToString(FTPFile ftpFile, int access) {
                        StringBuilder sb = new StringBuilder();
                        if (ftpFile.hasPermission(access, ftpFile.READ_PERMISSION)) {
                            sb.append('r');
                        } else {
                            sb.append('-');
                        }
                        if (ftpFile.hasPermission(access, ftpFile.WRITE_PERMISSION)) {
                            sb.append('w');
                        } else {
                            sb.append('-');
                        }
                        if (ftpFile.hasPermission(access, ftpFile.EXECUTE_PERMISSION)) {
                            sb.append('x');
                        } else {
                            sb.append('-');
                        }
                        return sb.toString();
                    }

                });

                PluginResult pluginResult = new PluginResult(PluginResult.Status.NO_RESULT);
                pluginResult.setKeepCallback(true);
                return true;
            }

        } catch (JSONException e) {
            callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.JSON_EXCEPTION, e.getMessage()));
        } catch (IOException e) {
            if (ftp.isConnected()) {
                try {
                    ftp.disconnect();
                } catch (IOException f) {
                    // do nothing
                }
            }
            callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.IO_EXCEPTION, e.getMessage()));
        } catch (Exception e) {
            callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.MALFORMED_URL_EXCEPTION, e.getMessage()));
        }
        return true;
    }
}


