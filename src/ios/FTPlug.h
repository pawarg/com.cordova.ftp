#import <Cordova/CDVPlugin.h>
@interface FTPlug : CDVPlugin
- (void) recursivelist: (CDVInvokedUrlCommand*) command;
- (void) list:(CDVInvokedUrlCommand *)command;
- (void) mkdir:(CDVInvokedUrlCommand *)command;
- (void) rename:(CDVInvokedUrlCommand *)command;
- (void) download:(CDVInvokedUrlCommand *)command;
- (void) upload:(CDVInvokedUrlCommand *)command;
@end
