#import "FTPlug.h"
#import "FTPKit.h"

@implementation FTPlug

NSString* recl(FTPClient* client, NSString* PATHNAME) {
    NSArray *contents = [client listContentsAtPath:PATHNAME showHiddenFiles:NO];
    NSString *jsondata = [[NSString alloc] init];
    if (contents) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd 'at' h:mm aaa"];
        for (FTPHandle *handle in contents) {
            if (handle.type == FTPHandleTypeFile) {
                @try {
                    NSString *jsonobject = [@"{\"path\":\"" stringByAppendingString:handle.path];
                    jsonobject = [jsonobject stringByAppendingString:@"\",\"type\":\"File\",\"user\":\""];
                    jsonobject = [jsonobject stringByAppendingString:handle.owner];
                    jsonobject = [jsonobject stringByAppendingString:@"\",\"name\":\""];
                    jsonobject = [jsonobject stringByAppendingString:handle.name];
                    jsonobject = [jsonobject stringByAppendingString:@"\",\"group\":\""];
                    jsonobject = [jsonobject stringByAppendingString:handle.group];
                    jsonobject = [jsonobject stringByAppendingString:@"\",\"permissions\":\""];
                    jsonobject = [jsonobject stringByAppendingString:handle.permissions];
                    jsonobject = [jsonobject stringByAppendingString:@"\",\"link\":\""];
                    jsonobject = [jsonobject stringByAppendingString:handle.link];
                    jsonobject = [jsonobject stringByAppendingString:@"\",\"size\":\""];
                    jsonobject = [jsonobject stringByAppendingString:[NSString stringWithFormat:@"%l", handle.size]];
                    jsonobject = [jsonobject stringByAppendingString:@"\",\"modified\":\""];
                    jsonobject = [jsonobject stringByAppendingString:[dateFormatter stringFromDate:handle.modified]];
                    jsonobject = [jsonobject stringByAppendingString:@"\"},"];
                    jsondata = [jsondata stringByAppendingString:jsonobject];
                }
                @catch (NSException *exception) {
                    NSLog(@"Exception1 %@", exception.reason);
                }
            } else if (handle.type == FTPHandleTypeDirectory) {
                @try {
                    NSString *jsonobject = [@"{\"path\":\"" stringByAppendingString:handle.path];
                    jsonobject = [jsonobject stringByAppendingString:@"\",\"type\":\"Dir\",\"user\":\""];
                    jsonobject = [jsonobject stringByAppendingString:handle.owner];
                    jsonobject = [jsonobject stringByAppendingString:@"\",\"name\":\""];
                    jsonobject = [jsonobject stringByAppendingString:handle.name];
                    jsonobject = [jsonobject stringByAppendingString:@"\",\"group\":\""];
                    jsonobject = [jsonobject stringByAppendingString:handle.group];
                    jsonobject = [jsonobject stringByAppendingString:@"\",\"permissions\":\""];
                    jsonobject = [jsonobject stringByAppendingString:handle.permissions];
                    jsonobject = [jsonobject stringByAppendingString:@"\",\"link\":\""];
                    jsonobject = [jsonobject stringByAppendingString:handle.link];
                    jsonobject = [jsonobject stringByAppendingString:@"\",\"size\":\""];
                    jsonobject = [jsonobject stringByAppendingString:[NSString stringWithFormat:@"%l", handle.size]];
                    jsonobject = [jsonobject stringByAppendingString:@"\",\"modified\":\""];
                    jsonobject = [jsonobject stringByAppendingString:[dateFormatter stringFromDate:handle.modified]];
                    jsonobject = [jsonobject stringByAppendingString:@"\",\"subdir\":["];
                    jsonobject = [jsonobject stringByAppendingString:recl(client, handle.path)];
                    jsonobject = [jsonobject stringByAppendingString:@"]},"];
                    jsondata = [jsondata stringByAppendingString:jsonobject];
                }
                @catch (NSException *exception) {
                    NSLog(@"Exception2 %@", exception.reason);
                }
            }
        }
    } else {
        return nil;
    }
    @try {
        if(jsondata != nil){
            //TODO just check comma at the end of string and remove it.
            int k = [jsondata length] - 1;
            unichar comma = [jsondata characterAtIndex:k];
            if(comma == ','){
                return [jsondata substringWithRange:NSMakeRange(0, k)];
            }
            return jsondata;
        }
    }
    @catch (NSException *exception) {
        NSLog(@"Exception3 %@", exception.reason);
    }

    return nil;
}

- (void)recursivelist:(CDVInvokedUrlCommand *)command {
    [self.commandDelegate runInBackground:^{
        CDVPluginResult *pluginResult = nil;
        NSDictionary *parameters = [command.arguments objectAtIndex:0];
        NSString *FTP_ADDRESS = (NSString *) [parameters objectForKey:@"ftphost"];
        NSNumber *PORT = (NSNumber *) [parameters objectForKey:@"port"];
        NSString *USERNAME = (NSString *) [parameters objectForKey:@"username"];
        NSString *PASSWORD = (NSString *) [parameters objectForKey:@"password"];
        NSString *PATHNAME = (NSString *) [parameters objectForKey:@"pathname"];
        FTPClient *client = [FTPClient clientWithHost:FTP_ADDRESS port:PORT username:USERNAME password:PASSWORD];
        NSString *json = recl(client, PATHNAME);
        json = [@"[" stringByAppendingString:json];
        json = [json stringByAppendingString:@"]"];
        NSLog(@"JSON %@", json);
        if(json==nil)
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
        else
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:json];

        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }];
}

- (void) list:(CDVInvokedUrlCommand *)command{
    [self.commandDelegate runInBackground:^{
        CDVPluginResult *pluginResult = nil;
        NSDictionary *parameters = [command.arguments objectAtIndex:0];
        NSString *FTP_ADDRESS = (NSString *) [parameters objectForKey:@"ftphost"];
        NSNumber *PORT = (NSNumber *) [parameters objectForKey:@"port"];
        NSString *USERNAME = (NSString *) [parameters objectForKey:@"username"];
        NSString *PASSWORD = (NSString *) [parameters objectForKey:@"password"];
        NSString *PATHNAME = (NSString *) [parameters objectForKey:@"pathname"];
        FTPClient *client = [FTPClient clientWithHost:FTP_ADDRESS port:PORT username:USERNAME password:PASSWORD];

        NSArray *contents = [client listContentsAtPath:PATHNAME showHiddenFiles:NO];
        NSString *jsondata = [[NSString alloc] init];
        if (contents) {
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd 'at' h:mm aaa"];
            for (FTPHandle *handle in contents) {
                if (handle.type == FTPHandleTypeFile) {
                    @try {
                        NSString *jsonobject = [@"{\"path\":\"" stringByAppendingString:handle.path];
                        jsonobject = [jsonobject stringByAppendingString:@"\",\"type\":\"File\",\"user\":\""];
                        jsonobject = [jsonobject stringByAppendingString:handle.owner];
                        jsonobject = [jsonobject stringByAppendingString:@"\",\"name\":\""];
                        jsonobject = [jsonobject stringByAppendingString:handle.name];
                        jsonobject = [jsonobject stringByAppendingString:@"\",\"group\":\""];
                        jsonobject = [jsonobject stringByAppendingString:handle.group];
                        jsonobject = [jsonobject stringByAppendingString:@"\",\"permissions\":\""];
                        jsonobject = [jsonobject stringByAppendingString:handle.permissions];
                        jsonobject = [jsonobject stringByAppendingString:@"\",\"link\":\""];
                        jsonobject = [jsonobject stringByAppendingString:handle.link];
                        jsonobject = [jsonobject stringByAppendingString:@"\",\"size\":\""];
                        jsonobject = [jsonobject stringByAppendingString:[NSString stringWithFormat:@"%l", handle.size]];
                        jsonobject = [jsonobject stringByAppendingString:@"\",\"modified\":\""];
                        jsonobject = [jsonobject stringByAppendingString:[dateFormatter stringFromDate:handle.modified]];
                        jsonobject = [jsonobject stringByAppendingString:@"\"},"];
                        jsondata = [jsondata stringByAppendingString:jsonobject];
                    }
                    @catch (NSException *exception) {
                        NSLog(@"Exception1 %@", exception.reason);
                    }
                } else if (handle.type == FTPHandleTypeDirectory) {
                    @try {
                        NSString *jsonobject = [@"{\"path\":\"" stringByAppendingString:handle.path];
                        jsonobject = [jsonobject stringByAppendingString:@"\",\"type\":\"Dir\",\"user\":\""];
                        jsonobject = [jsonobject stringByAppendingString:handle.owner];
                        jsonobject = [jsonobject stringByAppendingString:@"\",\"name\":\""];
                        jsonobject = [jsonobject stringByAppendingString:handle.name];
                        jsonobject = [jsonobject stringByAppendingString:@"\",\"group\":\""];
                        jsonobject = [jsonobject stringByAppendingString:handle.group];
                        jsonobject = [jsonobject stringByAppendingString:@"\",\"permissions\":\""];
                        jsonobject = [jsonobject stringByAppendingString:handle.permissions];
                        jsonobject = [jsonobject stringByAppendingString:@"\",\"link\":\""];
                        jsonobject = [jsonobject stringByAppendingString:handle.link];
                        jsonobject = [jsonobject stringByAppendingString:@"\",\"size\":\""];
                        jsonobject = [jsonobject stringByAppendingString:[NSString stringWithFormat:@"%l", handle.size]];
                        jsonobject = [jsonobject stringByAppendingString:@"\",\"modified\":\""];
                        jsonobject = [jsonobject stringByAppendingString:[dateFormatter stringFromDate:handle.modified]];
                        jsonobject = [jsonobject stringByAppendingString:@"\",\"subdir\":["];
                        jsonobject = [jsonobject stringByAppendingString:recl(client, handle.path)];
                        jsonobject = [jsonobject stringByAppendingString:@"]},"];
                        jsondata = [jsondata stringByAppendingString:jsonobject];
                    }
                    @catch (NSException *exception) {
                        NSLog(@"Exception2 %@", exception.reason);
                    }
                }
            }
        } else {
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
        }
        @try {
            if(jsondata != nil){
                int k = [jsondata length] - 1;
                unichar comma = [jsondata characterAtIndex:k];
                if(comma == ','){
                    jsondata = [jsondata substringWithRange:NSMakeRange(0, k)];
                }
                jsondata = [@"[" stringByAppendingString:jsondata];
                jsondata = [jsondata stringByAppendingString:@"]"];
                pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:jsondata];
            }
        }
        @catch (NSException *exception) {
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
        }
        NSLog(@"JSON %@", jsondata);
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }];
}

- (void) mkdir:(CDVInvokedUrlCommand *)command{
    [self.commandDelegate runInBackground:^{
        __block CDVPluginResult *pluginResult = nil;
        NSDictionary *parameters = [command.arguments objectAtIndex:0];
        NSString *FTP_ADDRESS = (NSString *) [parameters objectForKey:@"ftphost"];
        NSNumber *PORT = (NSNumber *) [parameters objectForKey:@"port"];
        NSString *USERNAME = (NSString *) [parameters objectForKey:@"username"];
        NSString *PASSWORD = (NSString *) [parameters objectForKey:@"password"];
        NSString *PATHNAME = (NSString *) [parameters objectForKey:@"pathname"];
        NSString *DIRPATH = (NSString *) [parameters objectForKey:@"dirpath"];
        FTPClient *client = [FTPClient clientWithHost:FTP_ADDRESS port:PORT username:USERNAME password:PASSWORD];
        BOOL success = [client directoryExistsAtPath:DIRPATH];
        if (! success) {
          BOOL success = [client createDirectoryAtPath:DIRPATH];
            if (! success) {
              pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsBool:false];
            }else{
              pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsBool:true];
            }
        }else{
          pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsBool:true];
        }
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }];
}

- (void) rename:(CDVInvokedUrlCommand *)command{
    [self.commandDelegate runInBackground:^{
        __block CDVPluginResult *pluginResult = nil;
        NSDictionary *parameters = [command.arguments objectAtIndex:0];
        NSString *FTP_ADDRESS = (NSString *) [parameters objectForKey:@"ftphost"];
        NSNumber *PORT = (NSNumber *) [parameters objectForKey:@"port"];
        NSString *USERNAME = (NSString *) [parameters objectForKey:@"username"];
        NSString *PASSWORD = (NSString *) [parameters objectForKey:@"password"];
        NSString *PATHNAME = (NSString *) [parameters objectForKey:@"pathname"];

        NSString *FROM = (NSString *) [parameters objectForKey:@"from"];
        NSString *TO = (NSString *) [parameters objectForKey:@"to"];

        FTPClient *client = [FTPClient clientWithHost:FTP_ADDRESS port:PORT username:USERNAME password:PASSWORD];
        BOOL success = [client renamePath:FROM to:TO];
        if (! success) {
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsBool:false];
        }else{
          pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsBool:true];
        }
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }];
}

- (void) download:(CDVInvokedUrlCommand *)command{
    [self.commandDelegate runInBackground:^{
        __block CDVPluginResult *pluginResult = nil;
        NSDictionary *parameters = [command.arguments objectAtIndex:0];
        NSString *FTP_ADDRESS = (NSString *) [parameters objectForKey:@"ftphost"];
        NSNumber *PORT = (NSNumber *) [parameters objectForKey:@"port"];
        NSString *USERNAME = (NSString *) [parameters objectForKey:@"username"];
        NSString *PASSWORD = (NSString *) [parameters objectForKey:@"password"];
        NSString *PATHNAME = (NSString *) [parameters objectForKey:@"pathname"];

        NSString *REMOTE = (NSString *) [parameters objectForKey:@"remote"];
        NSString *LOCAL = (NSString *) [parameters objectForKey:@"local"];
        NSLog(@"Remote: %@",REMOTE);
        NSLog(@"and Local: %@", LOCAL);
        FTPClient *client = [FTPClient clientWithHost:FTP_ADDRESS port:PORT username:USERNAME password:PASSWORD];
        BOOL success = [client downloadFile:REMOTE to:LOCAL progress:NULL];
        if (! success) {
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsBool:false];
        }else{
          pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsBool:true];
        }
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }];
}

- (void) upload:(CDVInvokedUrlCommand *)command{
    [self.commandDelegate runInBackground:^{
        __block CDVPluginResult *pluginResult = nil;
        NSDictionary *parameters = [command.arguments objectAtIndex:0];
        NSString *FTP_ADDRESS = (NSString *) [parameters objectForKey:@"ftphost"];
        NSNumber *PORT = (NSNumber *) [parameters objectForKey:@"port"];
        NSString *USERNAME = (NSString *) [parameters objectForKey:@"username"];
        NSString *PASSWORD = (NSString *) [parameters objectForKey:@"password"];
        NSString *PATHNAME = (NSString *) [parameters objectForKey:@"pathname"];

        NSString *REMOTE = (NSString *) [parameters objectForKey:@"remote"];
        NSString *LOCAL = (NSString *) [parameters objectForKey:@"local"];
        NSLog(@"Remote: %@",REMOTE);
        NSLog(@"and Local: %@", LOCAL);
        FTPClient *client = [FTPClient clientWithHost:FTP_ADDRESS port:PORT username:USERNAME password:PASSWORD];
        BOOL success = [client uploadFile:LOCAL to:REMOTE progress:NULL];
        if (! success) {
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsBool:false];
        }else{
          pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsBool:true];
        }
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }];
}

@end
