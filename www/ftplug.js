var cordova = require('cordova');
module.exports = {
    ftpclient: function () {
        var logins = {
            ftphost: "",
            port: "21",
            username: "anonymous",
            password: "anonymous",
            pathname: "/"
        };
        return {
            setup: function (ftphost, port, username, password, pathname) {
                if (typeof(ftphost) === "undefined") {
                    logins.ftphost = null;
                } else {
                    logins.ftphost = ftphost;
                }

                if (typeof(username) === "undefined") {
                    logins.username = "anonymous";
                } else {
                    logins.username = username;
                }

                if (typeof(password) === "undefined") {
                    logins.password = "anonymous";
                } else {
                    logins.password = password;
                }

                if (typeof(port) === "undefined") {
                    logins.port = "21";
                } else {
                    logins.port = port.toString();
                }

                if (typeof(pathname) === "undefined") {
                    logins.pathname = "/";
                } else {
                    logins.pathname = pathname.toString();
                }
            },
            

            listnames: function (pathname, success, error) {
                var params = logins;
                if (typeof(pathname) !== "undefined") {
                    params.remote = pathname.toString();
                }

                cordova.exec(function (data) {
                    success(data);
                }, function (err) {
                    error(err);
                }, "FTPlug", "listnames", [params]);
            },

            list: function (remote, success, error) {
                var params = logins;
                if (typeof(remote) !== "undefined") {
                    params.remote = remote.toString();
                }

                cordova.exec(function (data) {
                    success(data);
                }, function (err) {
                    error(err);
                }, "FTPlug", "list", [params]);
            },

            recursivelist: function (remote, success, error) {
                var params = logins;
                if (typeof(remote) !== "undefined") {
                    params.path = remote.toString();
                }

                cordova.exec(function (data) {
                    success(data);
                }, function (err) {
                    error(err);
                }, "FTPlug", "recursivelist", [params]);
            },

            recursivedirlist: function (remote, success, error) {
                var params = logins;
                if (typeof(remote) !== "undefined") {
                    params.path = remote.toString();
                }

                cordova.exec(function (data) {
                    success(data);
                }, function (err) {
                    error(err);
                }, "FTPlug", "recursivedirlist", [params]);
            },

            mkdir: function (dirpath, success, error) {
                var params = logins;
                if (typeof(dirpath) !== "undefined") {
                    params.dirpath = dirpath.toString();
                    cordova.exec(function (data) {
                        success(data);
                    }, function (err) {
                        error(err);
                    }, "FTPlug", "mkdir", [params]);
                }else{
                    error("Directory name required!");
                }
            },

            deletedir: function (dirname, success, error) {
                var params = logins;
                if (typeof(dirname) !== "undefined") {
                    params.dirname = dirname.toString();
                    cordova.exec(function (data) {
                        success(data);
                    }, function (err) {
                        error(err);
                    }, "FTPlug", "deletedir", [params]);
                }else{
                    error("Directory name required!");
                }
            },

            rename: function (from, to, success, error) {
                var params = logins;
                if (typeof(from) !== "undefined" && typeof(to) !== "undefined") {
                    params.from = from.toString();
		    params.to = to.toString();
                    cordova.exec(function (data) {
                        success(data);
                    }, function (err) {
                        error(err);
                    }, "FTPlug", "rename", [params]);
                }else{
                    error("File name required!");
                }
            },

            move: function (from, to, success, error) {
                var params = logins;
                if (typeof(from) !== "undefined" && typeof(to) !== "undefined") {
                    params.from = from.toString();
		    params.to = to.toString();
                    cordova.exec(function (data) {
                        success(data);
                    }, function (err) {
                        error(err);
                    }, "FTPlug", "rename", [params]);
                }else{
                    error("File name required!");
                }
            },

            deletefile: function (filename, success, error) {
                var params = logins;
                if (typeof(filename) !== "undefined") {
                    params.filename = filename.toString();
                    cordova.exec(function (data) {
                        success(data);
                    }, function (err) {
                        error(err);
                    }, "FTPlug", "deletefile", [params]);
                }else{
                    error("File name required!");
                }
            },

            download: function (source, destination, success, error) {
                var params = logins;
                if (typeof(source) !== "undefined" || typeof(destination) !== "undefined") {
                    params.remote = source.toString();
                    params.local = destination.toString();
                    cordova.exec(function (data) {
                        success(data);
                    }, function (err) {
                        error(err);
                    }, "FTPlug", "download", [params]);
                }else{
                    error("Source and destination name required!");
                }
            },

            upload: function (source, destination, success, error) {
                var params = logins;
                if (typeof(source) !== "undefined" || typeof(destination) !== "undefined") {
                    params.local = source.toString();
                    params.remote = destination.toString();
                    cordova.exec(function (data) {
                        success(data);
                    }, function (err) {
                        error(err);
                    }, "FTPlug", "upload", [params]);
                }else{
                    error("Source and destination name required!");
                }
            }
        };
    }
};
